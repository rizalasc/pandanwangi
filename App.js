import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { createStore } from 'redux';
import reducers from './components/Reducer'
import AppContainer from './AppContainer';

const store = createStore(reducers);
export default class App extends Component {
  constructor(props) {
    super(props);
    // console.log(store);
    this.state = { isLogin: false, isLoading: false }
    AsyncStorage.getItem('Mytoken').then(function (tokendata) {
      store.dispatch({
        type: "STORE_TOKEN",
        payload: tokendata
      })
    });
  }

  render() {
    return (
      <Provider store={store}>
        <AppContainer></AppContainer>
      </Provider>
    )
  }
}