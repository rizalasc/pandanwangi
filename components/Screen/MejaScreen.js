import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StatusBar, SafeAreaView, TextInput, ScrollView } from 'react-native';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import axios from '../Requester/AxiosHttp';
import MejaItem from './List/MejaItem';
import { SearchBar } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'underscore';


var { height, width } = Dimensions.get('screen');

class Meja extends Component {
    constructor(props) {

        super(props);
        this.state = {
            isLoading: true,
            mejas: [],
            mejaFetched: false,
            search: "",
            filtered_meja: []
        }
        this.getMeja = this.getMeja.bind(this);
        this.getMeja();
        this.search = this.search.bind(this);
        this.onSelectTable = this.onSelectTable.bind(this);
    }

    getMeja = () => {
        axios(this.props).get("/api/v1/meja").then((response) => {
            var meja = []
            // console.log(response.data.data);
            if (response.data.data) {
                meja = response.data.data
            }
            this.setState({ isLoading: false })
            this.setState({ mejas: meja, filtered_meja: meja, mejaFetched: true })
        })
    }

    async search(text) {
        var query = text;
        var searchedData = await _.filter(this.state.mejas, function (item) {
            return item.kode_meja.toLowerCase().indexOf(query.toLowerCase()) !== -1;
        })
        await this.setState({ filtered_meja: searchedData })
    }

    onSelectTable(kd, id, jumlah) {
        // console.log("INI TABLE " + kd + " " + id + " " + jumlah)
        const action = {
            type: 'SAVE_MEJA',
            kd: kd,
            id: id,
            jumlah: jumlah
        }
        this.props.dispatch(action);
        this.props.navigation.push("Home")
    }


    render() {
        return (
            <SafeAreaView style={{
                flexDirection: 'column',
                flex: 1,
                backgroundColor: "rgb(242, 242, 242)",
                alignItems: 'center',
                paddingTop: 20
            }}>
                <StatusBar barStyle='light-content' />
                <Spinner
                    cancelable={false}
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }}
                />
                <View style={{
                    margin: 20,
                    borderWidth: 0,
                    borderRadius: 10,
                    borderColor: '#fff',
                    width: width - 30,
                    justifyContent: "space-between",
                    // backgroundColor: 'white',
                    height: height / 15,
                    flexDirection: "row",
                    paddingRight: 10
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}><Image style={{
                        resizeMode: "stretch",
                        width: 65,
                        height: 65,
                    }} source={require('../../assets/home.png')}></Image>
                    </TouchableOpacity>

                    <Image style={{
                        resizeMode: "stretch",
                        width: 130,
                        height: 65,
                    }} source={require('../../assets/pandanwangi.png')}></Image>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'column',
                    width: width - 40,
                    height: height / 1.2,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 6.5,
                    elevation: 5,
                }}>
                    <View style={{
                        justifyContent: "space-between",
                        flexDirection: "row"
                    }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Meja :</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
                            <Image style={{ width: 20, height: 20 }} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingTop: 15, flexDirection: "column", paddingBottom: 10 }}>
                        <Text style={{ fontSize: 12, color: "grey", paddingBottom: 5 }}>Cari Meja</Text>
                        <TextInput
                            style={{
                                paddingHorizontal: 10,
                                height: 40,
                                backgroundColor: "rgb(242, 242, 242)",
                                borderRadius: 10
                            }}
                            placeholder="Kode Meja..."
                            placeholderTextColor="rgb(163, 163, 163)"
                            keyboardType={'numeric'}
                            onChangeText={(text) => {
                                this.search(text)
                            }}
                        />
                    </View>
                    <ScrollView horizontal={true} style={{ paddingBottom: 10 }}>
                        <FlatList
                            keyExtractor={(item, index) => item.id}
                            data={this.state.filtered_meja}
                            numColumns={3}
                            renderItem={({ item }) => (
                                <MejaItem table={item}
                                    navigation={this.props.navigation}
                                    onSelectTable={this.onSelectTable}
                                    kd={item.kode_meja}
                                    id={item.id}>
                                </MejaItem>
                            )} />
                    </ScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
};

export default connect(mapStateToProps)(Meja);