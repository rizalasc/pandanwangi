import React, { Component } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, SafeAreaView, StatusBar, ScrollView, Alert, AlertButton } from 'react-native';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import numeral from 'numeral';
import axios from '../Requester/AxiosHttp';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'underscore';


var { height, width } = Dimensions.get("screen");
class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            orderCart: this.props.cart.cart,
            sellitem: {
                "jenis_transaksi": "DINEIN",
                "status_pembayaran": "NOTPAID",
                "status_transaksi": "NEW",
                "customer_name": "",
                "jumlah_bayar": "",
                "franchise_kode": "RESTO6e288aca7336ee7737fb3c32e8748833",
                "order_object_type": "\\App\\Meja",
                "order_object": "",//ID Meja
                "frenchise_type": "RESTO",
                "selling_item": []
            },
            cartFormatted: []
        }
        this.parsedCart = this.parsedCart.bind(this);
        this.onOrder = this.onOrder.bind(this);
    }

    async parsedCart() {
        var cartFormatted = await _.map(this.state.orderCart, function (item) {
            return {
                "selling_item_id": item.id,
                "jumlah": Number(item.qty),
                "harga_beli": Number(item.hargaBeli)
            }
        })

        await this.setState({
            cartFormatted: cartFormatted
        })
    }

    async onOrder() {
        this.setState({
            isLoading: true
        });
        var fromSchema = this.state.sellitem;
        fromSchema.customer_name = this.props.name.name;
        fromSchema.jumlah_bayar = this.props.cart.subtotal;
        fromSchema.order_object = this.props.meja.meja.id_meja;
        fromSchema.selling_item = this.state.cartFormatted;
        // console.log(fromSchema);
        await axios(this.props).post('/api/v1/transaction/create', fromSchema).then(() => {
            this.setState({
                isLoading: false
            });
            Alert.alert(
                "Success",
                "Data berhasil dikirimkan",
                [
                    {
                        text: 'OK', onPress: () => {
                            this.props.dispatch({ type: 'CLEANCART' });
                            this.props.dispatch({ type: 'REMOVE_NAME' });
                            this.props.dispatch({ type: 'REMOVE_MEJA' });
                            this.props.navigation.navigate("Menu")
                        }
                    }
                ])
        })
    }

    componentDidMount() {
        this.parsedCart();
    }


    render() {
        return (
            <SafeAreaView style={{
                flexDirection: 'column',
                flex: 1,
                backgroundColor: "rgb(242, 242, 242)",
                alignItems: 'center',
                paddingTop: 20
            }}>
                <StatusBar barStyle="light-content" />
                <Spinner
                    cancelable={false}
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }}
                />
                <View style={{
                    margin: 20,
                    borderWidth: 0,
                    borderRadius: 10,
                    borderColor: '#fff',
                    width: width - 30,
                    justifyContent: "space-between",
                    // backgroundColor: 'white',
                    height: height / 15,
                    flexDirection: "row",
                    paddingRight: 10
                }}>
                    <TouchableOpacity><Image style={{
                        resizeMode: "stretch",
                        width: 65,
                        height: 65,
                    }} source={require('../../assets/home.png')}></Image>
                    </TouchableOpacity>

                    <Image style={{
                        resizeMode: "stretch",
                        width: 130,
                        height: 65,
                    }} source={require('../../assets/pandanwangi.png')}></Image>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'column',
                    width: width - 40,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 6.5,
                    elevation: 5,
                }}>
                    <View style={{
                        justifyContent: "space-between",
                        flexDirection: "row"
                    }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Order Confirmation</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
                            <Image style={{ width: 20, height: 20 }} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: "column", paddingTop: 10 }}>
                        <Text style={{ fontSize: 20 }}>Meja :</Text>
                        <Text style={{ fontSize: 23 }}>{this.props.meja.meja.kode_meja}</Text>
                        <Text style={{ fontSize: 20, paddingTop: 20 }}>Atas Nama</Text>
                        <Text style={{ fontSize: 23 }}>{this.props.name.name}</Text>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 15,
                            borderColor: "#000",
                            borderWidth: 1,
                            width: width - 80,
                            height: height - 525,
                            borderRadius: 8
                        }}>
                            <View>
                                <ScrollView horizontal={true}>
                                    <FlatList data={this.state.orderCart}

                                        numColumns={1}
                                        keyExtractor={item => item.id}
                                        renderItem={({ item }) =>
                                            <View style={{ paddingTop: 5, paddingStart: 5 }}>
                                                <Text style={{ fontSize: 18 }}>{item.name} X{numeral(item.qty).format('0')} @{numeral(item.subTotal).format('0,0')}</Text>
                                            </View>}
                                    ></FlatList>

                                </ScrollView>
                            </View>
                        </View>
                        <Text style={{ fontSize: 25, marginBottom: 10 }}>Total: Rp.{numeral(this.props.cart.subtotal).format('0,0')}</Text>
                        <TouchableOpacity style={{
                            borderRadius: 10,
                            width: width - 80,
                            height: height / 15,
                            backgroundColor: 'green',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} onPress={this.onOrder}>
                            <Text style={{
                                fontSize: 25,
                                color: 'white',
                                fontWeight: "bold"
                            }} >Place Order</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    const { auth } = state
    const { cart } = state
    const { meja } = state
    const { name } = state
    return { auth, cart, meja, name }
};

export default connect(mapStateToProps)(Order);