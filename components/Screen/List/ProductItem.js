import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native';
import _ from 'underscore';
import { connect } from 'react-redux';

var { height, width } = Dimensions.get('screen');
class ProductItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            qty: 0,
        }
        this.handleIncrement = this.handleIncrement.bind(this)
        this.handleDecrement = this.handleDecrement.bind(this)

    }

    componentDidUpdate() {
        var exsisting = _.findWhere(this.props.cart.cart, { id: this.props.products.id })
        if (exsisting) {
            if (exsisting.qty != this.state.qty) {
                this.setState({
                    qty: exsisting.qty
                })
            }
        } else if (exsisting == undefined) {
            if (this.state.qty != 0) {
                this.setState({
                    qty: 0,
                })
            }
        } else {
            if (this.state.qty != 0) {
                this.setState({
                    qty: 0,
                })
            }
        }
    }

    componentDidMount() {
        var exsisting = _.findWhere(this.props.cart.cart, { id: this.props.products.id })
        if (exsisting) {
            if (exsisting.qty != this.state.qty) {
                this.setState({
                    qty: exsisting.qty,
                })
            }
        } else {
            if (this.state.qty != 0) {
                this.setState({
                    qty: 0,
                })
            }
        }
        // console.log(this.props)
    }

    handleIncrement() {
        const { products } = this.props
        products.qty = this.state.qty + 1
        if (products.qty > this.props.products.stock) {
            Alert.alert("Pandan Wangi", "Stock tidak mencukupi")
        } else {
            var hargaJual = this.props.products.price
            products.hargaBeli = hargaJual
            products.subTotal = hargaJual * products.qty
            this.props.addCart(products)
        }
    }

    handleDecrement() {
        const { products } = this.props;
        if (this.state.qty > 1) {
            products.qty = this.state.qty - 1
            var hargaJual = this.props.products.price
            products.hargaBeli = hargaJual
            products.subTotal = hargaJual * products.qty
            this.props.addCart(products)
        } else {
            this.props.removeCart(products)
        }
    }

    render() {
        return (
            <View style={{
                backgroundColor: "white",
                margin: 5,
                borderWidth: 0,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOpacity: 0.5,
                shadowRadius: 5,
                shadowOffset: {
                    height: 5,
                    width: 0
                },
                width: (width - 50) / 2,
                elevation: 5
            }}>
                <Image
                    style={{ width: (width - 54) / 2, height: width > 500 ? 200 : 80, resizeMode: 'cover', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
                    source={{ uri: this.props.products.image }}
                />
                <View>
                    <View style={{
                        paddingBottom: 0,
                        borderTopWidth: 1,
                        borderColor: '#eee',
                        width: '100%',
                    }}>
                        <Text style={{
                            paddingLeft: 5,
                            paddingRight: 5,
                            justifyContent: 'center',
                            fontSize: 16,
                            color: '#fe9900',
                            fontSize: 18
                        }}> {this.props.products.name}</Text>
                    </View>
                    <View style={{
                        paddingBottom: 0,
                        width: '100%',
                    }}>
                        <Text style={{
                            paddingLeft: 10,
                            paddingRight: 5,
                            paddingBottom: 5,
                            justifyContent: 'center',
                            fontSize: 15,
                            color: '#fe9900'
                        }}>Rp. {this.props.products.price}</Text>
                    </View>
                    <View style={{
                        flexDirection: "row",
                        padding: 10,
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: 'green',
                        borderRadius: 10
                    }}>
                        <TouchableOpacity style={{ width: '15%', alignItems: 'center' }} onPress={this.handleDecrement}>
                            <Image style={{ width: 30, height: 30 }} source={require('../../../assets/minus-symbol.png')} />
                        </TouchableOpacity>
                        <View style={{ width: '70%', alignItems: 'center' }}>
                            <Text style={{ fontSize: 20, color: 'white', fontWeight: 'bold' }}>
                                {this.state.qty}
                            </Text>
                        </View>
                        <TouchableOpacity style={{ width: '15%', alignItems: 'center' }} onPress={this.handleIncrement}>
                            <Image style={{ width: 30, height: 30 }} source={require('../../../assets/plus-button.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { auth } = state
    const { cart } = state
    return { auth, cart }
};

const dispatchToProps = dispatch => ({
    addCart: (products) =>
        dispatch({ type: 'ADDCART', payload: products }),
    removeCart: (products) =>
        dispatch({ type: 'REMOVECART', payload: products }),
});

export default connect(mapStateToProps, dispatchToProps)(ProductItem)