import React, { Component, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { RadioButton } from 'react-native-simple-radio-button';
import ModalJmlOrang from '../../Modal/JmlOrang';
import ModalPindahMerge from '../../Modal/PindahMerge';
import ModalPindahMeja from '../../Modal/ModalPindah';
import axios from '../../Requester/AxiosHttp';

var { height, width } = Dimensions.get('screen');

class MejaItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            modalPindah: false,
            modalMerge: false,
            mejaEmpty: []
        }
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    showPindahMeja = () => {
        axios(this.props).get('/api/v1/meja/empty').then(response => {
            this.setState({
                mejaEmpty: response.data,
                modalPindah: !this.state.modalPindah
            })
        })
    }

    togglePindah = () => {
        this.setState({
            modalPindah: !this.state.modalPindah
        })
    }

    onPindah = (from) => {
        this.toggle({},
            this.props.navigation.push("PindahMeja", {
                from: from
            })
        )
    }

    onMerge = (parent) => {
        this.toggle({},
            this.props.navigation.push("MergeMeja", {
                parent: parent
            })
        )
    }

    getColor = (status) => {
        switch (status) {
            case 'EMPTY':
                return 'rgb(108,117,125)'
            case 'NEW':
                return 'rgb(23,162,184)'
            case 'COOKING':
                return 'rgb(255,193,7)'
            case 'CHECKING':
                return 'rgb(255,193,7)'
            case 'COOKED':
                return 'rgb(40,167,69)'
            case 'OCCUPIED':
                return 'rgb(0,123,255)'
        }
    }

    render() {
        var color = this.getColor(this.props.table.status_transaksi);
        return (
            <>
                <TouchableOpacity style={[{

                    margin: 5,
                    borderWidth: 0,
                    borderRadius: 10,
                    shadowColor: "#000",
                    shadowOpacity: 0.5,
                    shadowRadius: 5,
                    shadowOffset: {
                        height: 2,
                        width: 0
                    },
                    width: (width - 110) / 3,
                    height: (width - 110) / 3,
                    elevation: 5,
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "row"
                }, { backgroundColor: color }]} onPress={() => {
                    // const action = {
                    //     type: 'SAVE_MEJA',
                    //     payload: this.props.products.id
                    // }
                    // this.props.dispatch(action);
                    this.setState({
                        modal: !this.state.modal
                    })
                }}>
                    <Text style={{ color: "#FFF" }}>
                        {this.props.table.kode_meja}
                    </Text>
                </TouchableOpacity>
                {this.props.table.transaksi ?
                    <ModalPindahMerge show={this.state.modal} onCancel={this.toggle} onPindah={this.onPindah} onMerge={this.onMerge} id={this.props.table.id}></ModalPindahMerge>
                    :
                    <ModalJmlOrang show={this.state.modal} onCancel={this.toggle} onSave={this.props.onSelectTable} kd={this.props.kd} id={this.props.id} meja={this.props.table}></ModalJmlOrang>
                }


            </>
        )
    }
}


const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
}

export default connect(mapStateToProps)(MejaItem)