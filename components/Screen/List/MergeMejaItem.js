import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { Modal, Text, View, Dimensions, TouchableOpacity, Image, StyleSheet } from 'react-native';

var { height, width } = Dimensions.get('screen');

function MergeMejaItem(props) {
    const { item, onMerge, selected } = props;

    const selectID = (id) => {
        onMerge(id)
    }

    return (
        <TouchableOpacity style={[{

            margin: 5,
            borderWidth: 0,
            borderRadius: 10,
            shadowColor: "#000",
            shadowOpacity: 0.5,
            shadowRadius: 5,
            shadowOffset: {
                height: 2,
                width: 0
            },
            width: (width - 110) / 3,
            height: (width - 110) / 3,
            elevation: 5,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row"
        }, selected.includes(item.id) ? styles.primary : styles.secondary]} onPress={() => selectID(item.id)}>
            <Text style={{ color: "#FFF" }}>
                {item.kode_meja}
            </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    primary: {
        backgroundColor: 'rgb(0,123,255)'
    },
    secondary: {
        backgroundColor: 'rgb(108,117,125)'
    }
})


export default MergeMejaItem