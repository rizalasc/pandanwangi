import React, { Component } from 'react';
import { View, Text, SafeAreaView, Image, StatusBar, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { BackHandler } from 'react-native';

const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
};

var { height, width } = Dimensions.get('screen');
class Menu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            MenuListItems: [
                { name: "Buat Pesanan", action: "Home", type: "", type2: "", type3: "", type4: "" },
                { name: "Keluar", action: "Login", type: "LOGOUT_USER", type2: "REMOVE_NAME", type3: "REMOVE_MEJA", type4: "CLEANCART" },
                { name: "Kunci", action: "", type: "", type2: "", type3: "", type4: "" },
            ]
        }
    }

    async componentDidUpdate() {
        if (((this.props.auth.token == "") || (this.props.auth.token == null)) && this.props.auth.tokenDispatched) {
            this.props.navigation.push("Login")
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    render() {
        return (
            <SafeAreaView>
                <StatusBar barStyle='light-content' />
                <View style={{
                    height: '20%',
                    width: width,
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image source={require('../../assets/pandanwangi.png')} resizeMode="contain" style={{ height: 125 }} />
                </View>
                <View style={{
                    padding: 20,
                    height: '80%',
                    width: width,
                    backgroundColor: 'rgb(150, 196, 150)',
                    alignItems: 'center',
                    justifyContent: "center"
                }}>
                    <FlatList
                        data={this.state.MenuListItems}
                        keyExtractor={(item, index) => item.name}
                        numColumns={2}
                        renderItem={({ item }) =>
                            <TouchableOpacity style={{
                                borderRadius: 10,
                                backgroundColor: "white",
                                width: (width - 80) / 2,
                                margin: 10,
                                alignItems: "center",
                                flexDirection: "column",
                            }} onPress={() => {
                                this.props.dispatch({ type: item.type })
                                this.props.dispatch({ type: item.type2 })
                                this.props.dispatch({ type: item.type3 })
                                this.props.dispatch({ type: item.type4 })
                                this.props.navigation.push(item.action)

                            }}>
                                <Image source={require('../../assets/plus-button.png')} resizeMode="center" style={{
                                    height: 100,
                                    margin: 35,
                                    borderBottomWidth: 1,
                                    borderColor: "#000"
                                }} />
                                <Text style={{ height: 30, fontSize: 20, margin: 5, fontWeight: "bold" }}>{item.name}</Text>

                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        )
    }
}

export default connect(mapStateToProps)(Menu);