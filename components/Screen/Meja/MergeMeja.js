import React, { Component } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, TextInput, SafeAreaView, StatusBar, ScrollView, Alert, AlertButton } from 'react-native';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import numeral from 'numeral';
import axios from '../../Requester/AxiosHttp';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'underscore';
import PindahMejaItem from '../List/PindahMejaItem';
import MergeMejaItem from '../List/MergeMejaItem';

var { height, width } = Dimensions.get('screen');

class MergeMeja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            search: "",
            filtered_meja: [],
            selected: []
        }
        this.getData = this.getData.bind(this)
        this.onMerge = this.onMerge.bind(this)
        this.onSave = this.onSave.bind(this)
    }

    getData() {
        var params = this.props.navigation.state.params;
        axios(this.props).get("/api/v1/meja/not/" + params.parent).then(response => {
            this.setState({
                isLoading: false,
                data: response.data.data,
                filtered_meja: response.data.data
            })
        })
    }

    componentDidMount() {
        this.getData()
    }

    onMerge(selected) {
        this.setState({
            selected: [...this.state.selected, selected]
        }, () => console.log(this.state.selected))
    }

    onSave() {
        var params = this.props.navigation.state.params;
        var data = {}

        if (this.state.selected.length == 0) {
            Alert.alert(
                "Merge Meja",
                "Meja belum dipilih"
            )
            return
        }

        data.parent = params.parent;
        data.merged = this.state.selected

        axios(this.props).post('/api/v1/transaction/merge-meja', data).then(response => {
            this.props.navigation.push("Meja")
        })
    }

    render() {
        return (
            <SafeAreaView style={{
                flexDirection: 'column',
                flex: 1,
                backgroundColor: "rgb(242, 242, 242)",
                alignItems: 'center',
                paddingTop: 20
            }}>
                <StatusBar barStyle="light-content" />
                <Spinner
                    cancelable={false}
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }}
                />
                <View style={{
                    margin: 20,
                    borderWidth: 0,
                    borderRadius: 10,
                    borderColor: '#fff',
                    width: width - 30,
                    justifyContent: "space-between",
                    // backgroundColor: 'white',
                    height: height / 15,
                    flexDirection: "row",
                    paddingRight: 10
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}><Image style={{
                        resizeMode: "stretch",
                        width: 65,
                        height: 65,
                    }} source={require('../../../assets/home.png')}></Image>
                    </TouchableOpacity>

                    <Image style={{
                        resizeMode: "stretch",
                        width: 130,
                        height: 65,
                    }} source={require('../../../assets/pandanwangi.png')}></Image>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'column',
                    width: width - 40,
                    height: height / 1.4,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 6.5,
                    elevation: 5,
                }}>
                    <View style={{
                        justifyContent: "space-between",
                        flexDirection: "row"
                    }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Merge Meja</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
                            <Image style={{ width: 20, height: 20 }} source={require('../../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingTop: 15, flexDirection: "column", paddingBottom: 10 }}>
                        <Text style={{ fontSize: 12, color: "grey", paddingBottom: 5 }}>Cari Meja</Text>
                        <TextInput
                            style={{
                                paddingHorizontal: 10,
                                height: 40,
                                backgroundColor: "rgb(242, 242, 242)",
                                borderRadius: 10
                            }}
                            placeholder="Kode Meja..."
                            placeholderTextColor="rgb(163, 163, 163)"
                            keyboardType={'numeric'}
                            onChangeText={(text) => {
                                this.search(text)
                            }}
                        />
                    </View>
                    <ScrollView horizontal={true} style={{ paddingBottom: 10, height: height - 100 }}>
                        <FlatList
                            keyExtractor={(item, index) => item.id}
                            data={this.state.filtered_meja}
                            numColumns={3}
                            renderItem={({ item }) => (
                                <MergeMejaItem item={item} onMerge={this.onMerge} selected={this.state.selected}></MergeMejaItem>

                            )} />
                    </ScrollView>
                </View>
                <TouchableOpacity style={{
                    flex: 0.8,
                    marginTop: 10,
                    marginBottom: 10,
                    padding: 20,
                    backgroundColor: 'rgb(65,136,52)',
                    borderRadius: 15,
                    flexDirection: 'column',
                    width: width - 40,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 6.5,
                    elevation: 5,
                    justifyContent: "center",
                    alignItems: "center"
                }} onPress={() => this.onSave()}>
                    <Text style={{
                        fontSize: 30,
                        color: "#FFF",
                        fontWeight: "bold"
                    }}>Simpan</Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }

}

const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
}

export default connect(mapStateToProps)(MergeMeja)