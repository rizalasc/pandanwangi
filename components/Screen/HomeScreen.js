import React, { Component } from 'react';
import {
    View, Text, TextInput, Image, Alert, TouchableOpacity,
    StatusBar, SafeAreaView, FlatList, ScrollView
} from 'react-native';
import { Dimensions, AsyncStorage } from 'react-native';
import axios from '../Requester/AxiosHttp';
import Dialog from 'react-native-dialog';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { connect } from 'react-redux';
import ProducItem from './List/ProductItem';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar } from 'react-native-elements';

var { height, width } = Dimensions.get('screen');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            editname: false,
            name: '',
            products: [],
            productFetched: false,
        }
        this.getProducts = this.getProducts.bind(this)
        this.getProducts();
        // console.log(this.props.meja.meja)
    }

    getProducts = () => {
        // console.log(this.props.auth.token)
        axios(this.props).get("/api/v1/sellingitem").then((response) => {
            this.setState({ isLoading: false })
            var prod = []
            // console.log(response.data.data[0]);
            if (response.data.data) {
                prod = response.data.data;
            }
            this.setState({ products: prod, productFetched: true })
        }).catch((error) => {
            this.setState({ isLoading: false })
        });
    }


    render() {
        return (
            <SafeAreaView style={{
                flexDirection: 'column',
                flex: 1,
                backgroundColor: "rgb(242, 242, 242)",
                alignItems: 'center',
                paddingTop: 20
            }}>
                <StatusBar barStyle='light-content' />
                <Spinner
                    cancelable={false}
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }}
                />
                <Dialog.Container
                    visible={this.state.editname}>
                    <Dialog.Title>Atas Nama:</Dialog.Title>
                    <TextInput style={{
                        borderRadius: 10,
                        height: 40,
                        fontSize: 15,
                        paddingHorizontal: 10,
                        backgroundColor: "rgb(242,242,242)"
                    }} onChangeText={(nama) => {
                        this.setState({
                            name: nama
                        });
                    }} value={this.state.name}>
                    </TextInput>
                    <Dialog.Button label="Cancel" onPress={async () => {
                        if (this.state.name != "") {
                            this.setState({
                                editname: false
                            })
                        } else {
                            this.setState({
                                name: "",
                                editname: false
                            })
                        }
                    }} />
                    <Dialog.Button label="Confirm" onPress={async () => {
                        await this.setState({
                            editname: false
                        });
                        var that = this;
                        const action = {
                            type: 'SAVE_NAME',
                            payload: that.state.name
                        }
                        that.props.dispatch(action);
                    }} />
                </Dialog.Container>
                <View style={{
                    margin: 20,
                    borderWidth: 0,
                    borderRadius: 10,
                    borderColor: '#fff',
                    width: width - 30,
                    justifyContent: "space-between",
                    // backgroundColor: 'white',
                    height: height / 15,
                    flexDirection: "row",
                    paddingRight: 10
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Menu")}>
                        <Image style={{
                            resizeMode: "stretch",
                            width: 65,
                            height: 65,
                        }} source={require('../../assets/home.png')}></Image>
                    </TouchableOpacity>
                    <Image style={{
                        resizeMode: "stretch",
                        width: 130,
                        height: 65,
                    }} source={require('../../assets/pandanwangi.png')}></Image>
                </View>
                <View style={{
                    marginLeft: 20,
                    marginRight: 20,
                    marginBottom: 10,
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: '#fff',
                    width: width - 30,
                    backgroundColor: 'white',
                    height: height / 5,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 6.5,
                    elevation: 10,
                }}>
                    <View style={{
                        flexDirection: 'row',
                        paddingLeft: 20,
                        paddingTop: 15,
                        justifyContent: "space-between"
                    }}>
                        <Text style={{
                            fontSize: 25,
                            alignItems: 'flex-start'
                        }}>Meja : {this.props.meja.meja.kode_meja}</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Meja")}>
                            <Image style={{
                                height: 30,
                                width: 30,
                                marginEnd: 15,
                                alignItems: "flex-end"
                            }} source={require('../../assets/edit.png')}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        paddingLeft: 20,
                        paddingTop: 15,
                        justifyContent: "space-between"
                    }}>
                        <Text style={{
                            fontSize: 25,
                            alignItems: 'flex-start'
                        }}>A/n : {this.props.name.name}</Text>
                        <TouchableOpacity onPress={() => this.setState({
                            editname: true
                        })}>
                            <Image style={{
                                height: 30,
                                width: 30,
                                marginEnd: 15,
                                alignItems: "flex-end"
                            }} source={require('../../assets/edit.png')}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        alignItems: 'center',
                        marginTop: 10,
                    }}>
                        <TouchableOpacity style={{
                            borderRadius: 10,
                            width: width - 50,
                            height: height / 15,
                            backgroundColor: 'green',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} onPress={() => {
                            if (this.props.cart.cart.length < 1) {
                                Alert.alert("Warning", "Pilih makanan sebelum order")
                            } else {
                                this.props.navigation.navigate("Order")
                            }
                        }}>
                            <Text style={{
                                fontSize: 25,
                                color: 'white',
                                fontWeight: "bold"
                            }} >Place Order</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView horizontal={true} style={{ paddingBottom: 10 }}>
                    <FlatList
                        keyExtractor={(item, index) => item.id}
                        data={this.state.products}
                        numColumns={2}
                        renderItem={({ item }) => (
                            <ProducItem key={item.id} products={item} />
                        )}>

                    </FlatList>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    const { auth } = state
    const { meja } = state
    const { name } = state
    const { cart } = state
    return { auth, meja, name, cart }
};

export default connect(mapStateToProps)(Home);