import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import axios from './Requester/AxiosHttp';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Alert, KeyboardAvoidingView, View, TextInput,
    Text, StyleSheet, SafeAreaView, Keyboard, StatusBar, Image,
    TouchableOpacity, TouchableWithoutFeedback, BackHandler, AsyncStorage
} from 'react-native';

import { connect } from 'react-redux';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    logo: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        height: '40%',
        left: 0,
        right: 0,
        top: 0,
        backgroundColor: 'white',
    },
    form_login: {
        flex: 1,
        position: 'absolute',
        flexDirection: 'column',
        height: '60%',
        bottom: 0,
        left: 0,
        right: 0,
        padding: 20,
        backgroundColor: 'rgb(150, 196, 150)',
    },
    input: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        borderWidth: 0,
        borderRadius: 15,
        paddingHorizontal: 10,
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.2)',
    },
    btnLogin: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: 'rgb(24, 124, 32)',
        height: 50,
    }
});

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            account: {
                "grant_type": "password",
                "client_id": "2",
                "client_secret": "9kvcNAM7Mpba2hlhdAkaWWHcPlNXiS1feoPpy2QX",
                "username": "",
                "password": ""
            },
            txtUsername: '',
            usernameValid: false,
            errorEmail: 'Email is required.',
            txtPassword: '',
            passwordValid: false,
            errorPassword: 'Password is required.'
        };

        this.handleLogin = this.handleLogin.bind(this);
        // console.log(this.props.auth)
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    handleLogin = () => {
        this.setState({
            isLoading: true
        });
        var that = this;
        axios(that.props).post('/oauth/token', that.state.account).then(result => {
            that.setState({
                isLoading: false
            });
            // console.log(result)
            if (result.status == 200) {
                const action = {
                    type: 'STORE_TOKEN',
                    payload: result.data.access_token
                }
                that.props.dispatch(action)
                that.props.navigation.navigate('Menu')
                // that.props.navigation.pop();
                // that.props.navigation.push('Home');

            }

        }).catch((error) => {
            // console.log(error);
            if (error.response.status == 401) {
                Alert.alert('Pandan Wangi', 'Email / Password Anda Salah');
            } else if (error.response.status == 400) {
                Alert.alert('Pandan Wangi', 'Silahkan Isi Username dan Password');
            }
            that.setState({
                isLoading: false
            });
        })
    }

    render() {
        const { account } = this.state;
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle='light-content' />
                <Spinner
                    cancelable={false}
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }}
                />
                <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                    <KeyboardAvoidingView style={styles.container}>
                        <View style={styles.logo}>
                            <Image source={require('../assets/pandanwangi.png')} />
                        </View>
                        <View style={styles.form_login}>
                            <TextInput style={styles.input}
                                placeholder="Enter email address"
                                placeholderTextColor="rgb(58, 58, 58)"
                                returnKeyType="next"
                                keyboardType='email-address'
                                value={this.state.account.username}
                                onChangeText={(username) => {
                                    this.setState({
                                        account: { ...account, username },
                                        txtUsername: username
                                    });
                                }}
                                autoCorrect={false}
                                onSubmitEditing={() => this.refs.txtPassword.focus()} />
                            {this.state.usernameValid == true &&
                                <Text style={{ fontSize: 10, color: 'red', paddingHorizontal: 20 }}>{this.state.errorEmail}</Text>
                            }
                            <TextInput style={styles.input}
                                placeholder="Password"
                                placeholderTextColor="rgb(58, 58, 58)"
                                returnKeyType="go"
                                value={this.state.account.password}
                                onChangeText={(password) => this.setState({ account: { ...account, password }, password: password })}
                                secureTextEntry={true}
                                ref={"txtPassword"} />
                            {this.state.passwordValid == true &&
                                <Text style={{ fontSize: 10, color: 'red', paddingHorizontal: 20 }}>{this.state.errorPassword}</Text>
                            }
                            <TouchableOpacity style={styles.btnLogin} onPress={this.handleLogin}>
                                <Text style={{ color: 'white' }}>SIGN IN</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }
}
const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
};
export default connect(mapStateToProps)(Login);