import axios from 'axios';
import { AsyncStorage, Alert } from 'react-native';

var axiosInstance = null
var createRequest = function (props) {

    axiosInstance = axios.create({
        baseURL: "http://devdistriboost.gamma.co.id"
    });

    var token = props.auth ? props.auth.token : ""
    axiosInstance.defaults.headers.common = {
        "Authorization": "Bearer " + token,
        "Accept": "application/json"
    };

    // Add a request interceptor
    axiosInstance.interceptors.request.use(function (config) {
        // Do something before request is sent
        return config;
    }, function (error) {
        // //console.log(error)
        // Do something with request error
        return Promise.reject(error);
    });

    axiosInstance.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        console.log(error.response)
        if (error.response.status === 401) {
            return Promise.reject(error);
        } else if (error.response.status === 500) {

            return Promise.reject(error);
        } else {
            return Promise.reject(error);
        }
    });

    return axiosInstance
}

export default createRequest