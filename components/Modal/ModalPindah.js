import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { Modal, Text, View, Dimensions, TouchableOpacity, Image, FlatList, ScrollView } from 'react-native';
import axios from '../Requester/AxiosHttp';

var { height, width } = Dimensions.get("screen");

function ModalPindah(props) {
    const { show, data, id_from, onCancel } = props

    const [form, setForm] = useState({});
    const [selected, setSelected] = useState(null);

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={onCancel}
                visible={show}>
                <View style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <View style={{
                        // backgroundColor: "rgb(163,195,151)",
                        backgroundColor: "#FFF",
                        margin: 0,
                        borderRadius: 20,
                        width: width - 50,
                        height: (height / 1.2),
                        alignItems: "center",
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                        flexDirection: "column"
                    }}>

                    </View>
                </View>
            </Modal>
        </>
    )
}

export default ModalPindah