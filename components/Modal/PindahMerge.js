import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { Modal, Text, View, Dimensions, TouchableOpacity, Image } from 'react-native';

var { height, width } = Dimensions.get("screen")

function PindahMerge(props) {
    const { show, onCancel, onPindah, id, onMerge } = props

    const Pindah = () => {
        onPindah(id)
    }

    const Merge = () => {
        onMerge(id)
    }
    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={onCancel}
                visible={show}>
                <View style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <View style={{
                        backgroundColor: "rgb(163,195,151)",
                        margin: 0,
                        borderRadius: 20,
                        width: width - 250,
                        height: (height / 4),
                        alignItems: "center",
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                        flexDirection: "column"
                    }}>
                        <TouchableOpacity style={{
                            marginTop: 10,
                            borderRadius: 20,
                            width: width - 270,
                            flex: 1 / 3,
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column",
                            backgroundColor: "rgb(65,136,52)"
                        }} onPress={onCancel}>
                            <Text style={{
                                fontSize: 30,
                                color: "#FFF",
                                fontWeight: "bold"
                            }}>Edit Order</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            marginTop: 10,
                            borderRadius: 20,
                            width: width - 270,
                            flex: 1 / 3,
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column",
                            backgroundColor: "rgb(65,136,52)"
                        }} onPress={Pindah}>
                            <Text style={{
                                fontSize: 30,
                                color: "#FFF",
                                fontWeight: "bold"
                            }}>Pindah Meja</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            marginTop: 10,
                            marginBottom: 10,
                            borderRadius: 20,
                            width: width - 270,
                            flex: 1 / 3,
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column",
                            backgroundColor: "rgb(65,136,52)"
                        }} onPress={Merge}>
                            <Text style={{
                                fontSize: 30,
                                color: "#FFF",
                                fontWeight: "bold"
                            }}>Merge Meja</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </>
    )
}

export default PindahMerge