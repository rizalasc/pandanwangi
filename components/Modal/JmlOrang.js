import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { Modal, Text, View, Dimensions, TouchableOpacity, Image } from 'react-native';

var { height, width } = Dimensions.get("screen")

function JmlOrang(props) {

    const {
        meja,
        show,
        onCancel,
        onSave,
        kd, id
    } = props

    const [jumlah, setJumlah] = useState(1);

    async function handleIncrement() {
        if (jumlah <= parseInt(meja.kapasitas)) {
            await setJumlah(parseInt(jumlah) + 1)
        }
    }
    async function handleDecrement() {
        if (jumlah > 1) {
            await setJumlah(parseInt(jumlah) - 1)
        }
    }

    const save = () => {
        onCancel();
        onSave(kd, id, jumlah);
    }

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={onCancel}
                visible={show}>
                <View style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <View style={{
                        backgroundColor: "rgb(163,195,151)",
                        margin: 0,
                        borderRadius: 20,
                        width: width - 70,
                        height: height / 4,
                        alignItems: "center",
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                        flexDirection: "column"
                    }}>
                        <View style={{
                            width: width - 70,
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            justifyContent: "center",
                            alignItems: "center",
                            flex: .3,
                            backgroundColor: "rgb(65,136,52)"
                        }}>
                            <Text style={{
                                fontSize: 30,
                                color: "#FFF",
                                fontWeight: "bold"
                            }}>Jumlah Orang</Text>
                        </View>
                        <View style={{
                            width: width - 100,
                            marginTop: 30,
                            flex: .2,
                            flexDirection: "row",
                            padding: 10,
                            justifyContent: "center",
                            alignItems: "center",
                            backgroundColor: '#FFF',
                            borderRadius: 10
                        }}>
                            <TouchableOpacity style={{ width: '10%', borderRadius: 10, backgroundColor: "rgb(65,136,52)", alignItems: 'center' }} onPress={handleDecrement}>
                                {/* <Image style={{ width: 30, height: 30 }} source={require('../../assets/minus-symbol.png')} /> */}
                                <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}>
                                    -
                                </Text>
                            </TouchableOpacity>
                            <View style={{ width: '80%', alignItems: 'center' }}>
                                <Text style={{ fontSize: 20, color: '#000', fontWeight: 'bold' }}>
                                    {jumlah}
                                </Text>
                            </View>
                            <TouchableOpacity style={{ width: '10%', borderRadius: 10, backgroundColor: "rgb(65,136,52)", alignItems: 'center' }} onPress={handleIncrement}>
                                {/* <Image style={{ width: 30, height: 30 }} source={require('../../assets/plus-button.png')} /> */}
                                <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}>
                                    +
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            marginTop: 30,
                            flex: .4,
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                            <TouchableOpacity style={{
                                height: '80%',
                                margin: 10,
                                width: '30%',
                                borderRadius: 10,
                                backgroundColor: "rgb(65,136,52)",
                                justifyContent: "center",
                                alignItems: "center"
                            }} onPress={save}>
                                <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}>
                                    OK
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                height: '80%',
                                margin: 10,
                                width: '30%',
                                borderRadius: 10,
                                backgroundColor: "rgb(235, 45, 42)",
                                justifyContent: "center",
                                alignItems: "center"
                            }} onPress={onCancel} >
                                <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}>
                                    BATAL
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </>
    )
}


const mapStateToProps = (state) => {
    const { auth } = state
    return { auth }
}

export default connect(mapStateToProps)(JmlOrang)