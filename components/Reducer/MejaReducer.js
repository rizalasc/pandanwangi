const INITIAL_STATE = {
    meja: '',
}

const mejaReducer = (state = INITIAL_STATE, action) => {
    var newState = {};
    switch (action.type) {
        case 'SAVE_MEJA':
            newState = { meja: { "id_meja": action.id, "kode_meja": action.kd, "jml_orang": action.jumlah } }
            return newState;
        case 'REMOVE_MEJA':
            newState = { meja: '' }
            return newState;
        default:
            return state;
    }
}

export default mejaReducer