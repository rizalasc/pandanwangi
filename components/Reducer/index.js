import { combineReducers } from 'redux';
import authReducer from './AuthReducer';
import cartReducer from './CartReducer';
import mejaReducer from './MejaReducer';
import nameReducer from './NameReducer';


export default combineReducers({
    auth: authReducer,
    cart: cartReducer,
    meja: mejaReducer,
    name: nameReducer
});