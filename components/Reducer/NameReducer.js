const INITIAL_STATE = {
    name: '',
}

const nameReducer = (state = INITIAL_STATE, action) => {
    var newState = {};
    switch (action.type) {
        case 'SAVE_NAME':
            newState = { name: action.payload }
            return newState;
        case 'REMOVE_NAME':
            newState = { name: '' }
            return newState;
        default:
            return state;
    }
}

export default nameReducer