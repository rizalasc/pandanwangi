import { AsyncStorage } from "react-native";

const INITIAL_STATE = {
    token: "",
    tokenDispatched: false
}

_storeData = async (token) => {
    try {
        await AsyncStorage.setItem('Mytoken', token);
    } catch (error) {

    }
}

_removeData = async (token) => {
    try {
        await AsyncStorage.removeItem('Mytoken');
    } catch (error) {

    }
}

const authReducer = (state = INITIAL_STATE, action) => {
    var newState = {};
    switch (action.type) {
        case 'STORE_TOKEN':
            try {
                if (action.payload == undefined || action.payload == null) {
                    action.payload = ""
                }
                _storeData(action.payload)
            } catch (error) {

            }
            newState = { token: action.payload, tokenDispatched: true }
            return newState;
        case 'LOGOUT_USER':
            try {
                _removeData()
            } catch (error) {

            }
            newState = { token: null, tokenDispatched: true }
            return newState;
        default:
            return state;
    }
};
export default authReducer