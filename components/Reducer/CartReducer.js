import _ from 'underscore';

const INITIAL_STATE = {
    cart: [],
    subtotal: 0
}

const navigationReducer = (state = INITIAL_STATE, action) => {
    var newState = {};
    var subtotal = 0
    const {
        cart,
    } = state;

    switch (action.type) {
        case 'ADDCART':
            subtotal = 0;
            var exsisting = _.findIndex(cart, { id: action.payload.id })
            if (exsisting > -1 && cart.length > 0) {
                cart[exsisting] = action.payload
            } else {
                cart.push(action.payload)
            }

            subtotal = _.reduce(cart, function (memo, num) {
                return num.qty > 0 ? memo + (num.hargaBeli * num.qty) : 0;
            }, 0);
            var exsisting = _.findIndex(cart, { id: action.payload.id })
            newState = { cart, subtotal }
            // console.log(subtotal);
            // console.log(cart);
            return newState;
        case 'REMOVECART':
            subtotal = 0;
            var exsisting = _.findIndex(cart, { id: action.payload.id })
            cart.splice(exsisting, 1)
            subtotal = _.reduce(cart, function (memo, num) {
                return num.qty > 0 ? memo + (num.hargaBeli * num.qty) : 0;
            }, 0);
            newState = { cart, subtotal }
            // console.log(cart);
            return newState;
        case 'CLEANCART':
            return Object.assign({}, state, {
                cart: [],
                subtotal: 0
            })

        default:
            return state;

    }
};

export default navigationReducer