import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from './components/Login';
import HomeScreen from './components/Screen/HomeScreen';
import MejaScreen from './components/Screen/MejaScreen';
import MenuScreen from './components/Screen/MenuScreen';
import OrderScreen from './components/Screen/OrderScreen';
import PindahMejaScrn from './components/Screen/Meja/PindahMeja';
import MergeMejaScrn from './components/Screen/Meja/MergeMeja';

const AppNavigator = createStackNavigator({
    Home: { screen: HomeScreen, navigationOptions: { headerShown: false } },
    Login: { screen: LoginScreen, navigationOptions: { headerShown: false } },
    Meja: { screen: MejaScreen, navigationOptions: { headerShown: false } },
    Menu: { screen: MenuScreen, navigationOptions: { headerShown: false } },
    Order: { screen: OrderScreen, navigationOptions: { headerShown: false } },
    PindahMeja: { screen: PindahMejaScrn, navigationOptions: { headerShown: false } },
    MergeMeja: { screen: MergeMejaScrn, navigationOptions: { headerShown: false } }

},
    {
        initialRouteName: 'Menu'
    });

export default createAppContainer(AppNavigator);